<?php

/**
 * @file
 * Comment Smiley primary module file.
 *
 * It also apply on plain text comment box.
 */

/**
 * Function to replace all smiley text with related image.
 *
 * @param string $str_body
 *   Receive comment body text.
 *
 * @return string
 *   Return comment text with smiley images.
 */
function comment_smiley_replace($str_body) {
  $pattern = array();
  $replace = array();
  $img_lists = comment_smiley_load_images();
  foreach ($img_lists as $img) {
    $pattern[] = "/:$img->name:/";
    $output = array(
      '#theme' => 'image',
      '#path' => $img->uri,
      '#alt' => $img->name,
      '#attributes' => array('class' => ''),
    );
    $replace[] = drupal_render($output);
  }
  return preg_replace($pattern, $replace, $str_body);
}

/**
 * Implements hook_comment_view_alter().
 */
function comment_smiley_comment_view_alter(&$build) {
  $is_smiley_true = variable_get('comment_smiley_' . $build['#node']->type, FALSE);
  if ($is_smiley_true) {
    $build['comment_body'][0]['#markup'] = comment_smiley_replace($build['comment_body'][0]['#markup']);
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function comment_smiley_form_comment_form_alter(&$form, &$form_state, $form_id) {
  $is_smiley_true = variable_get('comment_smiley_' . $form['#node']->type, FALSE);
  if ($is_smiley_true) {
    $img_lists = comment_smiley_load_images();
    $arr_img = array();
    foreach ($img_lists as $img) {
      $arr_img[$img->name] = ":$img->name:";
    }
    drupal_add_js(array('comment_smiley' => array('slogo' => drupal_json_encode($arr_img))), array('type' => 'setting'));
    drupal_add_js(drupal_get_path('module', 'comment_smiley') . '/comment_smiley.js', array('scope' => 'footer'));
    $form['comment_body'][LANGUAGE_NONE]['#suffix'] = comment_smiley_with_commentbox();
  }
}

/**
 * Function to add smiley with comment box.
 *
 * @return string
 *    An string with images.
 */
function comment_smiley_with_commentbox() {
  $return = '';
  $arr_smiley_img = comment_smiley_load_images();
  foreach ($arr_smiley_img as $img) {
    $output = array(
      '#theme' => 'image',
      '#path' => $img->uri,
      '#alt' => $img->name,
      '#attributes' => array('class' => 'comment-smiley-logo'),
    );
    $return .= drupal_render($output) . ' ';
  }
  return $return;
}

/**
 * Function to load images from defaut folder.
 *
 * @return array
 *    An array of image information
 */
function comment_smiley_load_images() {
  $img_folder = drupal_get_path('module', 'comment_smiley') . '/default';
  $mask = '/\.png$/';
  $arr_smiley_img = file_scan_directory($img_folder, $mask);
  return $arr_smiley_img;
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function comment_smiley_form_node_type_form_alter(&$form, &$form_state) {
  if (isset($form['type'])) {
    $form['comment']['comment_smiley'] = array(
      '#type' => 'checkbox',
      '#title' => t('Comment Smiley'),
      '#description' => t('Apply smiley comment for this content type.'),
      '#default_value' => variable_get('comment_smiley_' . $form['#node_type']->type, FALSE),
    );
  }
}
