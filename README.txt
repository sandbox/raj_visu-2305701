PROBLEM:
--------
Some times we need to add our expressions with comments. If there is no
permission to add add html with our comments body then this module will help
you to add your expression without html.

SOLUTION:
---------
This module add set of images with your comment box, just click on this and
you get the some code with your comment box.

HOW TO USE:
------------
1. Install your module as usual. See more details on: 
   https://drupal.org/documentation/install/modules-themes/modules-7
2. Go to /admin/structure/types/manage/<content-type> 
   for example /admin/structure/types/manage/page in case of basic page.
3. Enable comment smiley under comment settings section.
