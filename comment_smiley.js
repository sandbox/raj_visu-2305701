(function ($) {
Drupal.behaviors.comment_smiley = {
  attach: function (context, settings) {
    slogo = $.parseJSON(Drupal.settings.comment_smiley.slogo);
    $('.comment-smiley-logo').click(function() {
        img = $(this).attr('alt');
        $('#edit-comment-body-und-0-value').val($('#edit-comment-body-und-0-value').val() + slogo[img]);
    })
  }
};
})(jQuery);
